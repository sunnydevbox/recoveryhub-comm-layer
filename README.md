## Running comm layer
1. Start by installing the dependencies
```
npm install
```
2. Make sure you set ```OT_SECRET``` and ```S3_SECRET``` in the path, you can ask for the values from the PM.  

in bash/unix based consoles you can usually do this by running
```
    export S3_SECRET = <value of secret>
    export OT_SECRET = <value of secret>
```

for windows try this [suggestion](https://stackoverflow.com/questions/9249830/how-can-i-set-node-env-production-on-windows)

3. after both env vars has been set, you can start the server via
```
npm start
```

Leave this process/window open when you are running RecoveryHub App.