const express = require('express')
const cors = require('cors')
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const OpenTok = require('opentok');
const bodyParser = require('body-parser');
const port = 5000;
const OT_INFO = { API_KEY: '46190132', API_SECRET: process.env.OT_SECRET };
const ot = new OpenTok(OT_INFO.API_KEY, OT_INFO.API_SECRET);
const S3 = require('aws-sdk/clients/s3');
const s3 = new S3({
	apiVersion: '2006-03-01',
	accessKeyId: 'AKIAJ6YYRJNSHF46IATQ',
	secretAccessKey: process.env.S3_SECRET
});

const corsOptions = {
	origin: [
		'http://localhost:4200',
		'http://app.recoveryhub.world/',
		'http://app.recoveryhub.ph/',
	]

}


app.use(bodyParser.json()); // for parsing application/json

let SESSIONS = [];
let USERS = {};

let findUser = id => SESSIONS.find(s => s.userId === id);

io.origins(['*:*']);
io.on('connection', function (socket) {
	let userId;
	// Registration phase
	io.to(socket.id).emit('whois');
	socket.on('iam', (payload) => {
		userId = payload.id;

		// console.log('transport opened:', userId, socket.id);
		if (USERS[userId]) {
			USERS[userId].socketId.push(socket.id);
		} else {
			USERS[userId] = {
				socketId: [socket.id],
				name: payload.name,
				connectedTo: undefined,
				inSession: false,
			};
		}

	})

	// Update connected status on disconnect
	socket.on('disconnect', (reason) => {
		// console.log('transport closed:', userId, socket.id);
		if (USERS[userId]) {
			const i = USERS[userId].socketId.indexOf(socket.id);
			USERS[userId].socketId.splice(i, 1);

			if (USERS[userId].inSession === socket.id) {
				USERS[userId].inSession = false;
			}
		}
	});

	socket.on('clientEvent', ({ action, data }) => {
		let me;
		const { receiver, caller, myId, callType, eventId } = data;
		const receiverId = receiver ? receiver.id : undefined;
		const callerId = caller ? caller.id : undefined;

		switch (action) {
			case 'connectToUser':
				// console.log('requester', caller)
				caller.socketId = socket.id;
				const isUserConnected = !!USERS[receiverId] && USERS[receiverId].socketId.length > 0;

				if (isUserConnected) {
					const isUserInSession = USERS[receiverId].inSession !== false;
					if (isUserInSession) {
						emitTo(socket.id, 'userStatusFeedback', { userStatus: 'userInSession' });
					} else {
						emitTo(socket.id, 'userStatusFeedback', { userStatus: 'requesting' });
						USERS[receiverId].socketId.forEach(function (sid) {
							emitTo(sid, 'connectionRequest', { caller, callType, eventId });
						});
					}
				} else {
					emitTo(socket.id, 'userStatusFeedback', { userStatus: 'userNotConnected' });
				}

				break;
			case 'connectionDeclined':
				USERS[callerId].socketId.forEach(id => {
					emitTo(id, 'connectionDeclined', { caller });
				});
				USERS[myId].socketId.forEach(id => {
					emitTo(id, 'closeRequest', { caller });
				});
				break;
			case 'connectionCanceled':
				USERS[myId].socketId.forEach(id => {
					emitTo(id, 'closeRequest', { caller });
				});
				USERS[receiverId].socketId.forEach(id => {
					emitTo(id, 'connectionCanceled', { caller });
				});

				break;
			case 'connectionApproved':
				// console.log('connectionApproved', myId, caller)

				emitTo(socket.id, 'creatingSession', true);
				USERS[myId].socketId.forEach(id => {
					if (id !== socket.id) {
						emitTo(id, 'closeRequest', true);
					}
				})

				emitTo(caller.socketId, 'creatingSession', true);
				const sessionOptions = {
					mediaMode: 'routed',
					archiveMode: 'always'
				};

				ot.createSession(sessionOptions, function (err, session) {
					console.log(session.sessionId)
					if (err) return console.log(err);

					const { sessionId } = session;
					const token = session.generateToken()
					const sessionConnectionInfo = { sessionId, token, key: OT_INFO.API_KEY };

					USERS[myId].inSession = socket.id;
					USERS[callerId].inSession = caller.socketId;

					emitTo(socket.id, 'sessionCreated', { sessionConnectionInfo });
					emitTo(caller.socketId, 'sessionCreated', { sessionConnectionInfo });
				});
				break;
			case 'closeSession':
				// console.log(action, myId)
				if (USERS[myId]) {
					USERS[myId].inSession = false;
				}
				break;
			default:
				console.log('UNHANDLED REQUEST:', action, data);
				break;
		}
	})
});

app.get('/', (req, res) => res.send('recoveryhub-tokbox running'))

app.options('/download-link/:id', cors(corsOptions));
app.get('/download-link/:id', cors(corsOptions), (req, res) => {
	if (req.params.id) {
		var params = {
			Bucket: "app-recoveryhub-world",
			Key: `${OT_INFO.API_KEY}/${req.params.id}/archive.mp4`
		};
		s3.getSignedUrl('getObject', params, (err, url) => {
			if (err) res.send(err);
			res.status(200).json({ url });
		});
	}
})

app.options('/archives', cors(corsOptions));
app.get('/archives', cors(corsOptions), (req, res) => {
	let sessionId = req.query.sessionId;
	try{
		ot.listArchives({ offset: 0, sessionId }, (error, archives, totalCount) => {
			if (error) res.status(500).send(error)
			else {
				res.json({
					archives,
					totalCount
				});
			}
		});
	} catch (e) {
		res.status(500).send(e);
	}
});


emit = (action, data) => io.emit('serverEvent', { action, data });
emitTo = (socketId, action, data) => io.to(socketId).emit('serverEvent', { action, data });

http.listen(port, () => console.log('recoveryhub-tokbox listening on port', port))